#!/usr/bin/python2.6
import sys, os, commands
import shutil
import getpass
import argparse
cmd='root-config --libdir'
ROOTSYS=commands.getoutput(cmd)
sys.path.append(ROOTSYS)
from ROOT import TFile,TTree,TChain,TBranch,TH1,TH1F,TList,TH3,TH3F,TLine

lumi_scale = 3000./139.

def xs_sf(hname):
  
    ##For the non-HH and non-H processes, the scale factor for cross section is 1.18
    ##Follow the recipes in this presentation: https://indico.cern.ch/event/1071645/#2-hl-lhc-study-kick-off
    sf = 1.18
    proc = " "
    if "_Sys" not in hname: proc = hname
    else: proc = hname.split("_")[0]

    if proc == "hhttbb": sf = 1.18
    if proc == "hhttbbVBFSM": sf = 1.19
    if proc == "ggFHtautau": sf = 1.13
    if proc == "VBFHtautau": sf = 1.13
    if "ZH" in proc: sf = 1.12
    if "WH" in proc: sf = 1.10
    if proc == "ttH": sf = 1.21

    ##Extras needed for kLambda studies
    if "hhttbbl" in proc: sf = 1.18
    if "hhttbbVBFl" in proc: sf = 1.19 
  
    return sf

def findZHF(hname):

    ##Find Z+HF processes, and scale them by 1.37 (norm factor derived from the control region)
    find = False
    if "Zbb" in hname or "Zttbb" in hname or "Zbc" in hname or "Zttbc" in hname or "Zcc" in hname or "Zttcc" in hname: find = True
    return find

    
def scaling(fin, fout):


    for k in fin.GetListOfKeys():

        # histograms in the Systematics folder
        if k.IsFolder() and k.GetName() == "Systematics":

            #continue
 
            dirin = fin.Get(k.GetName())
            dirout = fout.Get("Systematics")
            if not dirout:
                dirout = fout.mkdir("Systematics")
            dirout.cd()
           
            listsys = [] 
            for k2 in dirin.GetListOfKeys():
               
                sysname = k2.GetName().split("__")[0]
                listsys.append(sysname)
            listsys = list(dict.fromkeys(listsys))
       
            for sysname in listsys:
 
                h_up_scale, h_down_scale = None, None
                zhf_scale = 1.
                if findZHF(sysname): zhf_scale = 1.37
                xs_scale = xs_sf(sysname)
                proc = sysname.split("_")[0]
                #print sysname, proc
                if sysname == "Fake_SysttReweighting":
                    h_up_scale = dirin.Get(sysname)
                else:
                    h_up_scale = dirin.Get("{0}__1up".format(sysname))
                    h_down_scale = dirin.Get("{0}__1down".format(sysname))

                h_up_scale.Scale(xs_scale*lumi_scale*zhf_scale)
                if sysname == "Fake_SysttReweighting": h_up_scale.Write(h_up_scale.GetName())
                else: h_up_scale.Write("{0}__1up".format(sysname))
                if h_down_scale: 
                    h_down_scale.Scale(xs_scale*lumi_scale*zhf_scale) 
                    h_down_scale.Write("{0}__1down".format(sysname))
            fin.cd()
            fout.cd()
            fout.Flush()

        # histograms in the top level
        else:
    
            fin.cd()
            fout.cd()
            xs_scale = xs_sf(k.GetName())
            zhf_scale = 1.
            if findZHF(k.GetName()): zhf_scale = 1.37
            hist = k.ReadObj()
            hist.Scale(xs_scale*lumi_scale*zhf_scale)
            hist.Write(k.GetName())
            fout.Flush()

if __name__ == "__main__" :
    # Useage:
    # python scaleHist.py 13TeV_TauLH_2tag2pjet_0ptv_SM_NN.root 

    TH1.AddDirectory(False) 

    inFile = sys.argv[1]
    outFile = sys.argv[2] if len(sys.argv) > 2 else inFile.replace('.root', ".run2sys.root")
    fin = TFile(inFile)
    fout = TFile(outFile, "RECREATE")    
    scaling(fin, fout)
    fin.Close()
